-- Create a Common Table Expression (CTE) to rank customers based on total sales
WITH ranked_sales AS ( 
    SELECT 
    cust_id, 
    channel_id, 
    EXTRACT(YEAR FROM time_id) AS sale_year, 
    ROUND(SUM(amount_sold), 2) AS total_sales, 
    RANK() OVER (PARTITION BY channel_id, EXTRACT(YEAR FROM time_id) ORDER BY SUM(amount_sold) DESC) AS sales_rank 
  FROM 
    sh.sales 
  WHERE 
    EXTRACT(YEAR FROM time_id) IN (1998, 1999, 2001) 
  GROUP BY 
    cust_id, 
    channel_id, 
    sale_year 
) 
-- Select the relevant columns for the final sales report
SELECT
  sales_rank,
  cust_id AS customer_id, 
  channel_id AS sales_channel, 
  sale_year, 
  total_sales 
FROM 
  ranked_sales 
WHERE 
  sales_rank <= 300 
ORDER BY 
  sales_channel, 
  sale_year, 
  sales_rank;
